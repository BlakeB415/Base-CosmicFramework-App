<?php

use CosmicFramework\API\CosmicAccounts;
use CosmicFramework\MVC\View;

$klein->respond('GET', '/user/[:username]', function ($response) {
    require_once(__DIR__."/../models/user_model.php");
    $username = $response->username;
    $user = UserModel::GetUserByUsername($username);

    $videos = UserModel::FetchVideos(["limit" => 3, "offset" => 5]);

    //View without controller
    $view = new View("home", __DIR__ . "/../views");
    $view->setItems(["username" => $response->username]);
    $view->render();
});

$klein->respond('GET', '/video/[:video_name]', function ($response) {
    require(__DIR__."/../../config.php");
    require_once(__DIR__."/../controllers/video_controller.php");

    $base_url = $config['application']['base_url'];
    //View with controller
    $video_controller = new VideoController();
    $video_controller->addView(new View("video", __DIR__ . "/../views"));

    //Run logic from controller
    $video_controller->logic($response->video_name);
    $video_controller->render();
});

