<?php

use CosmicFramework\MVC\Controller;

class VideoController extends Controller {
    public function logic($video_name) {

        //Insert logic for video name
        $video_name = $video_name;

        $this->view->setItems(["video_name" => $video_name]);
    }
}