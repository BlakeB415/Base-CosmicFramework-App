<?php

use CosmicFramework\MVC\Database;

class UserModel extends Database {
    public static function GetUserByID($user_id) {
        return self::query("SELECT * FROM users WHERE user_id = :user_id;", [":user_id" => $user_id]);
    }
    public static function GetUserByUsername($user_name) {
        return self::query("SELECT * FROM users WHERE user_name LIKE :user_id;", [":user_name" => $user_name]);
    }

    public static function FetchVideos($params = null) {

        $base_sql = "SELECT * FROM videos";
        $sql = self::MYSQLStringConstructor($base_sql, $params);

        return self::query($sql);

    }
}