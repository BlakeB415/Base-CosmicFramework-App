<?php

require_once __DIR__. '/vendor/autoload.php';

use CosmicFramework\MVC\Database;

require('config.php');

Database::init($config["application"]["database"]["host"], $config["application"]["database"]["database"], $config["application"]["database"]["username"], $config["application"]["database"]["password"]);

$klein = new \Klein\Klein();

require("app/routes/routes.php");
require("app/routes/api.php");
require("app/routes/auth.php");

$klein->dispatch();